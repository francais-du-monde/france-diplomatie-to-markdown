# france-diplomatie-to-markdown

Convert [France Diplomatie](http://www.diplomatie.gouv.fr/) HTML pages to Markdown and include them in the
[Discourse-based forum of Français du monde](https://forum.francais-du-monde.fr/).

## Usage

```bash
./harvest_html.py -c -p ../france-diplomatie-services-aux-citoyens-html/
./html_to_markdown.py -c -p ../france-diplomatie-services-aux-citoyens-html/ ../france-diplomatie-services-aux-citoyens-markdown/
./markdown_to_discourse.py -k DISCOURSE_API_KEY -u DISCOURSE_API_USERNAME -v ../france-diplomatie-services-aux-citoyens-markdown/ https://forum.francais-du-monde.fr/
```
