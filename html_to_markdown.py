#! /usr/bin/env python3


# france-diplomatie-to-markdown -- Convert France Diplomatie HTML pages to Markdown
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
# https://git.framasoft.org/francais-du-monde/france-diplomatie-to-markdown
#
# france-diplomatie-to-markdown is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# france-diplomatie-to-markdown is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Convert "Services aux citoyens" HTML pages from diplomatie.gouv.fr to Markdown files."""


import argparse
import json
import logging
import os
import re
import subprocess
import sys
import urllib.parse

import lxml.html


app_name = os.path.splitext(os.path.basename(__file__))[0]
indented_element_re = re.compile('(?ims)^\s+<')
li_start_re = re.compile('(?ims)<li>\s+')
li_stop_re = re.compile('(?ims)\s+</li>')
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source_dir', help = 'path of source directory containing HTML files')
    parser.add_argument('target_dir', help = 'path of target directory for Markdown files')
    parser.add_argument('-c', '--commit', action = 'store_true', default = False, help = 'commit Markdown pages')
    parser.add_argument('-p', '--push', action = 'store_true', default = False, help = 'push commit')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = 'increase output verbosity')
    args = parser.parse_args()

    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    assert os.path.exists(args.source_dir)
    # Cleanup target directory.
    if os.path.exists(args.target_dir):
        for filename in os.listdir(args.target_dir):
            if filename.startswith('.'):
                continue
            if filename != 'map.json' and not filename.endswith('.md'):
                continue
            os.remove(os.path.join(args.target_dir, filename))
    else:
        os.makedirs(args.target_dir)

    with open(os.path.join(args.source_dir, 'map.json'), encoding = 'utf-8') as map_file:
        urls_mapping = json.load(map_file)
        external_redirection_by_url = urls_mapping['external']
        urls_by_html_filename = urls_mapping['internal']
        urls_by_markdown_filename = {
            '{}.md'.format(os.path.splitext(html_filename)[0]): urls
            for html_filename, urls in urls_by_html_filename.items()
            }
        main_url_by_markdown_filename = {
            markdown_filename: urls[0]
            for markdown_filename, urls in urls_by_markdown_filename.items()
            }
        markdown_filename_by_url = {
            url: markdown_filename
            for markdown_filename, urls in urls_by_markdown_filename.items()
            for url in urls
            }

    base_url = 'http://www.diplomatie.gouv.fr/fr/'
    for filename in os.listdir(args.source_dir):
        if filename.startswith('.'):
            continue
        if not filename.endswith('.html'):
            continue
        with open(os.path.join(args.source_dir, filename), encoding = 'utf-8') as html_file:
            html = html_file.read()
            html_element = lxml.html.document_fromstring(html)
            article_elements = html_element.xpath('//article')
            # if len(article_elements) == 0:
            #     # This page is a redirection. Ignore it.
            #     continue
            assert len(article_elements) == 1, filename
            article_element = article_elements[0]
            article_filename = '{}.md'.format(os.path.splitext(filename)[0])
            article_path = os.path.join(args.target_dir, article_filename)

            # Remove comments.
            for comment_element in article_element.xpath('.//comment()'):
                assert comment_element.tail is None or not comment_element.tail.strip(), filename
                comment_element.getparent().remove(comment_element)
            # Remove scripts.
            for script_element in article_element.xpath('.//script'):
                assert script_element.tail is None or not script_element.tail.strip(), filename
                script_element.getparent().remove(script_element)
            # Remove links to Facebook, Twitter, etc.
            for partage_element in article_element.xpath('.//section[@class="partage"]'):
                assert partage_element.tail is None or not partage_element.tail.strip(), filename
                partage_element.getparent().remove(partage_element)
            # Remove short news.
            for breve_element in article_element.xpath('.//div[@id="breve"]'):
                assert breve_element.tail is None or not breve_element.tail.strip(), filename
                breve_element.getparent().remove(breve_element)
            # Remove script-based blocks.
            for noscript_element in article_element.xpath('.//div[@class="noscript"]'):
                assert noscript_element.tail is None or not noscript_element.tail.strip(), filename
                noscript_element.getparent().remove(noscript_element)
            # Replace titrep divs with strong element.
            for titrep_element in article_element.xpath('.//div[@class="titrep"]'):
                titrep_element.tag = 'strong'
            # Replace framed blocks with blockquote.
            for xpath in (
                    './/div[@class="bloc_dernieres_minutes"]',
                    './/div[@class="home_dernieres_minutes_img_main"]',
                    './/div[@class="pt"]',
                    './/div[@class="texteencadre-spip spip"]',
                    './/div[@class="titre_dernier_minutes"]',
                    ):
                for block_element in article_element.xpath(xpath):
                    block_element.tag = 'blockquote'
            # Remove blocks but keep their content.
            for xpath in (
                    './/div[@align="center"]',
                    './/div[@class="ajaxbloc"]',
                    './/div[@class="caption_diapo"]',
                    './/div[@class="caption_text"]',
                    './/div[@class="center"]',
                    './/div[@class="chapo"]',
                    './/div[@class="clearfix"]',
                    './/div[@class="diaporama galleryview"]',
                    './/div[@class="document_doc"]',
                    './/div[@class="document_image"]',
                    './/div[@class="home_dernieres_minutes_img"]',
                    './/div[@class="home_dernieres_minutes_title"]',
                    './/div[@class="items"]',
                    './/div[@class="liens_utiles"]',
                    './/div[@class="mais_aussi"]',
                    './/div[@class="rubrique_speciale"]',
                    './/div[@class="slideshow galleryview"]',
                    './/div[@class="spip"]',
                    './/div[@class="spip_note"]',
                    './/div[@class="supplement_colonne"]/div',  # Must be before next line!
                    './/div[@class="supplement_colonne"]',
                    './/div[@class="texte"]',
                    './/div[starts-with(@class, "cadre_wrapper")]',
                    './/div[starts-with(@class, "spip_document")]',
                    './/div[starts-with(@class, "texte texte")]',
                    './/div[@id="entete_rub"]',
                    './/div[@id="fiche_pays"]',
                    './/div[@id="home_dernieres_minutes_panel"]',
                    './/div[@id="navigation_interne"]',
                    './/div[@id="sommaire"]',
                    './/div[starts-with(@id, "nb")]',
                    './/div[@style="text-align:justify"]',
                    './/section[@class="alert_cav"]',
                    './/section[@class="rubrique_speciale"]',
                    './/section[@id="navigation_interne"]',
                    ):
                for block_element in article_element.xpath(xpath):
                    assert block_element.tail is None or not block_element.tail.strip(), (filename, xpath)
                    if block_element.text is not None and block_element.text.strip():
                        assert all(
                            element.tag not in ('div', 'p', 'section', 'ul')
                            for element in block_element
                            ), (filename, xpath)
                        block_element.tag = 'p'
                    elif all(
                            element.tag not in ('div', 'p', 'section', 'ul')
                            for element in block_element
                            ):
                        block_element.tag = 'p'
                    else:
                        parent_element = block_element.getparent()
                        div_index = parent_element.index(block_element)
                        parent_element.remove(block_element)
                        for index, element in enumerate(block_element, div_index):
                            parent_element.insert(index, element)
            # Extract title.
            for h1_element in article_element.xpath('.//h1')[:1]:
                assert h1_element.tail is None or not h1_element.tail.strip(), filename
                assert h1_element.text is not None and h1_element.text.strip(), filename
                assert len(h1_element) == 0, filename
                article_title = h1_element.text.strip()
                h1_element.getparent().remove(h1_element)
            # Remove empty blocks.
            for xpath in (
                    './/div',
                    './/p',
                    ):
                for block_element in article_element.xpath(xpath):
                    if len(block_element) == 0 and (block_element.text is None or not block_element.text.strip()):
                        assert block_element.tail is None or not block_element.tail.strip(), (filename, xpath)
                        block_element.getparent().remove(block_element)

            if len(article_element.xpath('.//div')) > 0 or len(article_element.xpath('.//section')) > 0:
                log.warning('Markdown file {} still contains blocks'.format(article_path))

            # Convert URLs to paths of Markdowwn files orto remote URLs (redirections) to full URLs.
            for xpath, attribute_name in (
                    ('.//*[@href]', 'href'),
                    ('.//*[@src]', 'src'),
                    ):
                for link_element in article_element.xpath(xpath):
                    link_url = link_element.attrib[attribute_name]
                    link_full_url = urllib.parse.urljoin(base_url, link_element.attrib[attribute_name])
                    link_markdown_filename = markdown_filename_by_url.get(link_full_url)
                    if link_markdown_filename is None:
                        external_url = external_redirection_by_url.get(link_full_url)
                        if external_url is None:
                            if link_full_url != link_url:
                                link_element.attrib[attribute_name] = link_full_url
                        else:
                            link_element.attrib[attribute_name] = external_url
                    else:
                        link_element.attrib[attribute_name] = link_markdown_filename

            article = lxml.html.tostring(article_element, encoding = 'unicode').split('>', 1)[1].rsplit('<', 1)[0]
            article = indented_element_re.sub('<', article)
            for tag in ('li', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'table', 'ul'):
                article = article.replace('><{}'.format(tag), '>\n<{}'.format(tag))
            article = li_start_re.sub('<li>', article)
            article = li_stop_re.sub('</li>', article)
            article = article.replace('\t', '  ')
            article = article.strip()
            while ' \n' in article:
                article = article.replace(' \n', '\n')
            while '\n\n\n' in article:
                article = article.replace('\n\n\n', '\n\n')
            article = """# {}

{}

----

<small>Cette fiche est issue de [France Diplomatie]({}). Elle est actualisée automatiquement tous les jours par un [programme](https://framagit.org/francais-du-monde/france-diplomatie-to-markdown). Et contrairement à la fiche de France Diplomatie, vous pouvez y apporter vos compléments d'information, vos commentaires et vos corrections.</small>
""".format(
                article_title, article, main_url_by_markdown_filename[article_filename])
            with open(article_path, 'w') as article_file:
                article_file.write(article)

    with open(os.path.join(args.target_dir, 'map.json'), 'w', encoding = 'utf-8') as map_file:
        urls_mapping = dict(
            external = external_redirection_by_url,
            internal = urls_by_markdown_filename,
            )
        json.dump(urls_mapping, map_file, ensure_ascii = False, indent = 2, sort_keys = True)

    if args.commit:
        subprocess.check_call(['git', 'add', '.'], cwd = args.target_dir)
        process = subprocess.Popen(['git', 'commit', '-a', '-m Nouvelle conversion'], cwd = args.target_dir,
            stderr = subprocess.PIPE, stdout = subprocess.PIPE)
        output, error = process.communicate()
        if process.returncode == 0:
            if args.push:
                subprocess.check_call(['git', 'push', 'origin', 'master'], cwd = args.target_dir)
        else:
            assert process.returncode == 1 and b"nothing to commit" in output, \
                'An error {} occurred while committing:\noutput:\n{}\nerror:\n{}'.format(
                    process.returncode, output, error)

    return 0


if __name__ == "__main__":
    sys.exit(main())
