#! /usr/bin/env python3


# france-diplomatie-to-markdown -- Convert France Diplomatie HTML pages to Markdown
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2016 Paula Forteza & Emmanuel Raviart
# https://git.framasoft.org/francais-du-monde/france-diplomatie-to-markdown
#
# france-diplomatie-to-markdown is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# france-diplomatie-to-markdown is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Harvest "Services aux citoyens" HTML pages from diplomatie.gouv.fr."""


import argparse
import errno
import json
import logging
import os
import subprocess
import sys
import time
import urllib.error
import urllib.parse
import urllib.request

import lxml.html
from slugify import slugify


app_name = os.path.splitext(os.path.basename(__file__))[0]
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('target_dir', help = 'path of target directory for harvested HTML pages')
    parser.add_argument('-c', '--commit', action = 'store_true', default = False, help = 'commit HTML pages')
    parser.add_argument('-p', '--push', action = 'store_true', default = False, help = 'push commit')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = 'increase output verbosity')
    args = parser.parse_args()

    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    # Cleanup target directory.
    if os.path.exists(args.target_dir):
        for filename in os.listdir(args.target_dir):
            if filename.startswith('.'):
                continue
            if filename != 'map.json' and not filename.endswith('.html'):
                continue
            os.remove(os.path.join(args.target_dir, filename))
    else:
        os.makedirs(args.target_dir)

    external_redirection_by_url = {}
    base_url = 'http://www.diplomatie.gouv.fr/fr/'
    harvested_urls = set()
    remaining_urls = set([
        'http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/',
        ])
    urls_by_filename = {}  # Note: the main URL is the first one of the list, the others are redirections.
    while remaining_urls:
        page_url = remaining_urls.pop()
        if page_url in harvested_urls:
            continue
        harvested_urls.add(page_url)

        for retry in range(3):
            log.info('Harvesting page {}...'.format(page_url))
            try:
                response = urllib.request.urlopen(page_url, timeout = 20)
            except urllib.error.URLError as e:
                # Connection timed out.
                log.info('  An exception occurred: {}. Sleeping before retrying...'.format(e))
                time.sleep(10)
            else:
                break
        else:
            log.error("Can't harvest page {}. Skipping it.".format(page_url))
            continue
        response_url = response.geturl()
        if not response_url.startswith(base_url):
            # An external redirection has occurred. Don't harvest page.
            external_redirection_by_url[page_url] = response_url
            continue

        page_slug = slugify(response_url[len(base_url):])
        while len(page_slug) > 250:
            page_slug = page_slug.rsplit('-', 1)[0]
        page_filename = '{}.html'.format(page_slug)
        filename_urls = urls_by_filename.setdefault(page_filename, [])
        filename_urls.append(page_url)
        if response_url != page_url:
            if response_url in harvested_urls:
                assert response_url in filename_urls
                continue
            harvested_urls.add(response_url)
            assert response_url not in filename_urls
            filename_urls.insert(0, response_url)  # The response_url is the canonical URL for filename => put it first.

        html = response.read().decode("utf-8")
        with open(os.path.join(args.target_dir, page_filename), 'w', encoding = 'utf-8') as page_file:
            page_file.write(html)

        html_element = lxml.html.document_fromstring(html)
        for a_element in html_element.xpath('//ul/li[@class="premier_rubrique"]//a'):
            link_url = urllib.parse.urljoin(base_url, a_element.attrib['href'])
            assert link_url.startswith(base_url), link_url
            remaining_urls.add(link_url)

    with open(os.path.join(args.target_dir, 'map.json'), 'w', encoding = 'utf-8') as map_file:
        urls_mapping = dict(
            external = external_redirection_by_url,
            internal = urls_by_filename,
            )
        json.dump(urls_mapping, map_file, ensure_ascii = False, indent = 2, sort_keys = True)

    if args.commit:
        subprocess.check_call(['git', 'add', '.'], cwd = args.target_dir)
        process = subprocess.Popen(['git', 'commit', '-a', '-m Nouvelle récolte'], cwd = args.target_dir,
            stderr = subprocess.PIPE, stdout = subprocess.PIPE)
        output, error = process.communicate()
        if process.returncode == 0:
            if args.push:
                subprocess.check_call(['git', 'push', 'origin', 'master'], cwd = args.target_dir)
        else:
            assert process.returncode == 1 and b"nothing to commit" in output, \
                'An error {} occurred while committing:\noutput:\n{}\nerror:\n{}'.format(
                    process.returncode, output, error)

    return 0


if __name__ == '__main__':
    sys.exit(main())
